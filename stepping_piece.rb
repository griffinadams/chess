require_relative 'piece.rb'

class SteppingPiece < Piece

  def moves
    on_board = self.class::DELTAS.map do |(dx, dy)|
      [dx + @pos[0], dy + @pos[1]]
    end.select { |pos| @board.on_board?(pos) }

    on_board.select { |pos| @board.empty?(pos) || @board.capturable?(pos, @color) }
  end
end
