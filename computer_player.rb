require_relative 'player.rb'

class ComputerPlayer < Player

  attr_accessor :board

  def initialize(name, color)
    super
    @board = nil
  end

  def get_move
    all_valid_moves = []

    @board.pieces(self.color).each do |piece|
      piece.valid_moves.each do |valid_move|
        all_valid_moves << [piece.pos, valid_move]
      end
    end

    sleep(0.5)
    all_valid_moves.sample


    # priority list for moves
    # 1) get us in check mate
    # 2) capture move (in order of strength of piece)
    # 3) get us in check (but this could leave the to_check piece exposed)
    # 4) avoid hanging
    # 5) develop pieces (get pieces out) ~ attributes to moved to pieces
    # 6) move in to center of board
    # 7) castle ~ king checked_before? boolean
  end

  def mating_move?

  end
end
