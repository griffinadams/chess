require_relative 'pieces/pawn.rb'
require_relative 'pieces/rook.rb'
require_relative 'pieces/bishop.rb'
require_relative 'pieces/queen.rb'
require_relative 'pieces/king.rb'
require_relative 'pieces/knight.rb'
require 'colorize'
require 'byebug'
require_relative 'move_error.rb'

class Board
  SIZE = 8
  ROWS = SIZE.downto(1).to_a
  COLS = ROWS.map { |num| (num + "A".ord - 1).chr }.reverse
  BACK_RANK = [Rook, Knight, Bishop, Queen, King, Bishop, Knight, Rook]


  def initialize
    @board = Array.new(SIZE) { Array.new(SIZE) }
    @captured_pieces = []
  end

  def render
    SIZE.times do |idx|
      print "#{ROWS[idx]}| "
      SIZE.times do |idy|
        if self[[idx, idy]].nil?
          print "[ ]"
        else
          display_char, display_color = self[[idx, idy]].display
          print "[#{display_char.colorize(display_color)}]"
        end
      end
      print "\n"
    end
    print "    "
    SIZE.times { print "_  "}
    print "\n    "
    COLS.each { |col| print "#{col}  " }

    render_captured_pieces
    print "\n\n"
    nil
  end

  def render_captured_pieces
    @captured_pieces.each_with_index do |captured_piece, idx|
      display_char, display_color = captured_piece.display
      print "\n   " if idx % (SIZE - 1) == 0
      print "[#{display_char.colorize(display_color)}]"
    end
  end

  def move(start_pos, end_pos, current_color)
    move_errors(start_pos, end_pos, current_color)
    update_board(start_pos, end_pos)
    nil
  end

  def update_board(start_pos, end_pos)
    if can_castle?(start_pos, end_pos)
      castle(start_pos, end_pos)
    elsif pawn_to_promote?(start_pos, end_pos)
      Queen.new(self, end_pos, self[start_pos].color)
      self[start_pos] = nil
    else
      @captured_pieces << self[end_pos] unless empty?(end_pos)
      self[end_pos] = self[start_pos]
      self[end_pos].pos = end_pos
      self[start_pos] = nil
    end

    if self[end_pos].is_a?(King) || self[end_pos].is_a?(Rook)
      self[end_pos].moved = true
    end
  end

  def pawn_to_promote?(start_pos, end_pos)
    self[start_pos].is_a?(Pawn) && (end_pos[0] == 0 || end_pos[0] == 7)
  end

  def can_castle?(king_pos, rook_pos)
    king = self[king_pos]

    if rook_pos[1] < king_pos[1]
      rook = self[[rook_pos[0], rook_pos[1] - 2]]

    else
      rook = self[[rook_pos[0], rook_pos[1] + 1]]
    end

    return false unless king.is_a?(King) && rook.is_a?(Rook)
    return false if king.moved? || rook.moved?

    if rook.pos[1] < king.pos[1]
      !king.move_into_check?([king_pos[0], king_pos[1] - 1]) && empty?([king_pos[0], king_pos[1] - 1])
    else
      !king.move_into_check?([king_pos[0], king_pos[1] + 1]) && empty?([king_pos[0], king_pos[1] + 1])
    end

  end

  def castle(start_pos, end_pos)
    offsets = (start_pos[1] < end_pos[1] ? [1, 1] : [-2, -1])
    king_pos, rook_pos = start_pos, [end_pos[0], end_pos[1] + offsets[0]]
    self[[start_pos[0], start_pos[1] + offsets[1]]] = self[rook_pos]
    self[end_pos] = self[king_pos]
    self[king_pos] = self[rook_pos] = nil
  end

  def on_board?(move)
    move.all? { |pos| pos.between?(0, SIZE - 1) }
  end

  def empty?(pos)
    self[pos].nil?
  end

  def blocked?(pos, color)
    !empty?(pos) && self[pos].color == color
  end

  def capturable?(pos, color)
    !empty?(pos) && self[pos].color != color
  end

  def invalid?(start_pos, end_pos)
    !self[start_pos].valid_moves.include?(end_pos)
  end

  def in_check?(color)
    begin
      king = squares.find { |square| square.is_a?(King) && square.color == color}
      king_pos = king.pos
    rescue
      debugger
    end

    pieces(toggle_color(color)).any? do |piece|
      piece.moves.include?(king_pos)
    end
  end

  def over?(color)
    checkmate?(color) || stalemate?(color) || draw?
  end

  def checkmate?(color)
    pieces(color).none? { |piece| piece.valid_moves.count > 0 } &&
      in_check?(color)
  end

  def stalemate?(color)
    pieces(color).none? { |piece| piece.valid_moves.count > 0 } &&
      !in_check?(color)
  end

  def draw?
    pieces(:b).count == 1 && pieces(:w).count == 1
  end

  def pieces(color)
    squares.select { |square| !square.nil? && square.color == color }
  end

  def squares
    @board.flatten
  end

  def [](pos)
    row, col = pos
    @board[row][col]
  end

  def []=(pos, val)
    row,col = pos
    @board[row][col] = val
  end

  def dup
    board_copy = Board.new

    0.upto(SIZE - 1) do |idx|
      0.upto(SIZE - 1) do |idy|
        unless self[[idx, idy]].nil?
          board_copy[[idx, idy]] = self[[idx, idy]].dup
          board_copy[[idx, idy]].board = board_copy
        end
      end
    end

    board_copy
  end

  def populate
    sides = { b: [0, 1], w: [SIZE - 1, SIZE - 2] }

    place_piece = Proc.new do |piece, rank, file, color|
      piece.new(self, [rank, file], color)
    end

    sides.each do |color, rank|
      SIZE.times do |file|
        place_piece.call(BACK_RANK[file], rank[0], file, color)
        place_piece.call(Pawn,            rank[1], file, color)
      end
    end

    nil
  end

  private

  def toggle_color(color)
    color == :w ? :b : :w
  end

  def move_errors(start_pos, end_pos, current_color)
    raise MoveError, "No piece there!" if self[start_pos].nil?
    raise MoveError, "Not your piece!" if self[start_pos].color != current_color
    raise MoveError, "You must move" if start_pos == end_pos
    raise MoveError, "Invalid move!" if invalid?(start_pos, end_pos)
    raise MoveError, "Can't move into check" if self[start_pos].move_into_check?(end_pos)
  end
end
