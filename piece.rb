require 'byebug'
class Piece
  attr_accessor :pos, :board
  attr_reader :color

  def initialize(board, pos, color)
    @board, @pos, @color = board, pos, color
    add_to_board
  end

  def add_to_board
    @board[self.pos] = self
  end

  def moves
    # raise ImplementationError
  end

  def valid_moves
    moves.reject do |move|
      move_into_check?(move)
    end
  end

  def move_into_check?(move)
    board_copy = @board.dup
    board_copy.update_board(self.pos, move)
    board_copy.in_check?(self.color)
  end

  def display_color
    self.color == :w ? :red : :black
  end

end
