require_relative 'piece.rb'
require 'byebug'

class SlidingPiece < Piece

  def moves
    moves = []
    self.class::DELTAS.each do |(dx, dy)|
      1.upto(7) do |k|
        move = [@pos[0] + (dx * k), @pos[1] + (dy * k)]

        if !@board.on_board?(move) || @board.blocked?(move, @color)
          break
        elsif @board.empty?(move)
          moves << move
        elsif @board.capturable?(move, @color)
          moves << move
          break
        end

      end
    end

    moves
  end

end
