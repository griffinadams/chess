require_relative 'player.rb'
require_relative 'board.rb'

class HumanPlayer < Player

  def get_move
    puts "#{self.name.capitalize}: Please input a move."

    input = parse_input(gets.chomp.split.map { |pos| pos.split('') })
    unless input.all? { |pos| pos.all? {|num| num.between?(0, 7) } }
      raise "Position invalid."
    end

    input
  end

  private

  def parse_input(input)
    input.map do |pos|
        pos[0], pos[1] = Board::SIZE - pos[1].to_i, pos[0].downcase.ord - "a".ord
    end
  end

end
