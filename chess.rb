#!/usr/bin/env ruby

require_relative 'board.rb'
require_relative 'human_player.rb'
require_relative 'computer_player.rb'

require 'byebug'
class Chess
  attr_reader :board

  def initialize(white, black)
    @white, @black = white, black
    @board = Board.new
    @board.populate
  end

  def play
    current_player = @white
    @board.render

    until @board.over?(current_player.color)
      begin
        start_pos, end_pos = current_player.get_move
        @board.move(start_pos, end_pos, current_player.color)
      rescue MoveError => e
        puts e.message.colorize(:white).colorize(:background => :red)
        retry
      end
      @board.render

      current_player = (current_player == @white ? @black : @white )
    end

    if @board.checkmate?(current_player.color)
      puts "#{current_player.color.capitalize} is in checkmate!"
    elsif @board.stalemate?(current_player.color)
      puts "Stalemate on #{current_player.color}'s turn."
    elsif @board.draw?
      puts "Draw on #{current_player.color.capitalize}'s turn."
    end
  end

end

if __FILE__ == $PROGRAM_NAME
  puts "0: Human vs. Human"
  puts "1: Human vs. Computer"
  puts "2: Computer vs. Computer"
  case gets.chomp.to_i
  when 0
    puts "Player 1 (white): please input your name!"
    white = HumanPlayer.new(gets.chomp, :w)
    puts "Player 2 (black): please input your name!"
    black = HumanPlayer.new(gets.chomp, :b)
    game = Chess.new(white, black)
    game.play
  when 1
    puts "Player 1 (white): please input your name!"
    white = HumanPlayer.new(gets.chomp, :w)
    black = ComputerPlayer.new("Tammy", :b)
    game = Chess.new(white, black)
    black.board = game.board
    game.play
  when 2
    white = ComputerPlayer.new("Tammy", :w)
    black = ComputerPlayer.new("Hal", :b)
    game = Chess.new(white, black)
    black.board = white.board = game.board
    game.play
  else
    puts "Not an option!"
  end
end
