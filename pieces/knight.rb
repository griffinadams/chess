require_relative '../stepping_piece.rb'

class Knight < SteppingPiece

  DELTAS = [
    [-2, -1],
    [-2,  1],
    [-1, -2],
    [-1,  2],
    [ 1, -2],
    [ 1,  2],
    [ 2, -1],
    [ 2,  1]
  ]

  def display
    ["\u2658", display_color]
  end

end
