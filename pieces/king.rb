require_relative '../stepping_piece.rb'

class King < SteppingPiece

  DELTAS = [
    [-1, -1],
    [ 1,  1],
    [-1,  1],
    [ 1, -1],
    [-1,  0],
    [ 0,  1],
    [ 1,  0],
    [ 0, -1],
  ]

  CASTLE_DELTAS = [
    [0, -2],
    [0, 2]
    ]

  attr_accessor :moved

  def initialize(board, pos, color)
    super
    @moved = false
  end

  def moves
    castle_moves = CASTLE_DELTAS.map do |(dx, dy)|
      [dx + self.pos[0], dy + self.pos[1]]
    end.select do |move|
      @board.can_castle?(self.pos, move)
    end
    castle_moves + super
  end

  def moved?
    @moved
  end

  def display
    ["\u2654", display_color]
  end

end
