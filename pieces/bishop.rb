require_relative '../sliding_piece.rb'

class Bishop < SlidingPiece

  DELTAS = [
    [-1, -1],
    [ 1,  1],
    [-1,  1],
    [ 1, -1]
  ]

  def display
    ["\u2657", display_color]
  end

end
