require_relative '../sliding_piece.rb'

class Rook < SlidingPiece

  DELTAS = [
    [-1,  0],
    [ 0,  1],
    [ 1,  0],
    [ 0, -1]
  ]

  attr_accessor :moved

  def initialize(board, pos, color)
    super
    @moved = false
  end

  def moved?
    @moved
  end

  def display
    ["\u2656", display_color]
  end

end
