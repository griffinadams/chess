require_relative '../sliding_piece.rb'

class Queen < SlidingPiece

  DELTAS = [
    [-1, -1],
    [ 1,  1],
    [-1,  1],
    [ 1, -1],
    [-1,  0],
    [ 0,  1],
    [ 1,  0],
    [ 0, -1]
  ]

  def display
    ["\u2655", display_color]
  end

end
