require_relative '../piece.rb'

class Pawn < Piece

  def initialize(board, pos, color)
    super
    @dx = (@color == :w ? -1 : 1)
  end

  def display
    ["\u2659", display_color]
  end

  def moves
    straight_moves + captures
  end

  def straight_moves
    moves = []

    moves << [self.pos[0] + @dx, self.pos[1]]
    moves << [self.pos[0] + @dx * 2, self.pos[1]] unless moved?

    moves.select { |move| @board.empty?(move) }
  end

  def captures
    potential_captures = [[self.pos[0] + @dx, self.pos[1] - 1],
                          [self.pos[0] + @dx, self.pos[1] + 1]]

    potential_captures.select { |pos| @board.capturable?(pos, @color) }
  end

  def moved?
    self.color == :w ? self.pos[0] < 6 : self.pos[0] > 1
  end

end
